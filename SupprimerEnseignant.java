
	import java.awt.Color;

	import java.awt.event.ActionEvent;
	import java.awt.event.ActionListener;
	import java.awt.Dimension;
	import javax.swing.JButton;
	import javax.swing.JFrame;
	import javax.swing.JLabel;
	import javax.swing.JOptionPane;
	import javax.swing.JScrollPane;
	import javax.swing.JTextArea;
	import javax.swing.JTextField;
	import javax.swing.JPanel;
	import javax.swing.BoxLayout;
	import javax.swing.Box;
	



	/**
	 *SupprimerFenetre Class
 * allows to define and open the java class :
 * 
 *    - Allows to delete a new teacher in the  enseignant table by
 * the name, the first name, the department's entry
 *    - Allows to display all the the teachers in the console
 * @author (Toure et Diagne)
 * @version 1.0
 * */
	 


	public class SupprimerEnseignant extends JFrame implements ActionListener {
		
	private static final long serialVersionUID = 1L;

		/** the container : it takes the various components graphical
		 * SupprimerFenetre
		 */
	
		private JPanel containerPanel;

		/**
		 * 
		 *  text box declaration for all attributes for the Creer and supprimer frame
		 */
		private JTextField textFieldNom;
		private JTextField textFieldPrenom;
		private JTextField textFieldQualification;
		private JTextField textFieldIdentifiant;

		private JLabel labelNom;
		private JLabel labelPrenom;
		private JLabel labelQualification;
		private JLabel labelIdentifiant;
		
		
		
		
		/**
		 * 
		 * the label field definition for all attributes for the 2 frames
			*/
		
		/**
		 *  the Buttons boutonQuitter, boutonAjouter and boutonRechercher allow to delete a teacher and to quit the application
		 */
		private JButton boutonQuitter;
		private JButton boutonSupprimer;
		
		
		

		
		JTextArea zoneTextListEnseignant;

		/**
		 * Scroll area for the text box
		 */
		JScrollPane zoneDefilement;

		/**
		 * instance of the EnseignantDAO class: it allows the associations
		 */
		private EnseignantSupprimerDAO monEnseignantSupprimerDAO;
		
		/**
		 * the constructor sets its frame and its components- it allows to display the frame
		 */
		

		
		public SupprimerEnseignant() {
			
			/**
			 *   the SupprimerDAO class instantiation
			 */
			this.monEnseignantSupprimerDAO = new EnseignantSupprimerDAO();

			/**
			 *we give a title to our frame
			 */
			 
			this.setTitle("Menu de Suppression d'enseignant");
			
			/**
			 * initialization of the frame size
			 */ 
			this.setSize(400, 400);

			
			containerPanel = new JPanel();
			/**
			 * container creation
			 */ 


			
			containerPanel.setLayout(new BoxLayout(containerPanel,
					BoxLayout.PAGE_AXIS));
			// choix de la couleur pour le conteneur
			
			containerPanel.setBackground(Color.white);
			/**
			 * layout container choice
			 * it allows to do the position of the elements
			 *	BoxLayout allows for example to do the position of the elements on a column 
			 *	we use ( PAGE_AXIS )
			 */ 

			new JTextField();
			
			
			/**
			 * allows to give a name to the text field
			 */ 
			
			
			/**
			 * calling of the text field
			 */ 
			
			textFieldNom = new JTextField();
			textFieldPrenom = new JTextField();
			textFieldQualification = new JTextField();
			textFieldIdentifiant = new JTextField();
			
		
			
			/**
			 * the button's calling
			 */ 
			
			
			boutonQuitter= new JButton("Quitter");
			boutonSupprimer = new JButton("Supprimer");
			
			
			
			
			
			
			/**
			 * allows to write above the text field
			 */ 
			
			
				labelNom = new JLabel("Nom:");
				labelPrenom = new JLabel("Prenom :");
				labelQualification = new JLabel("Qualification  :");
				labelIdentifiant=new JLabel("Identifiant:");
			
			

				/**
				 * the text list allows to increase or to decrease the text area size
				 */ 
				
			
			
			

			zoneTextListEnseignant = new JTextArea(10, 20);
			zoneDefilement = new JScrollPane(zoneTextListEnseignant);
			zoneTextListEnseignant.setEditable(false);

			
			/**
			 * create  a space between the  label and the text Field
			 */ 
				
			
					containerPanel.add(Box.createRigidArea(new Dimension(0, 5)));
					
			
					/**
					 * allows to display an empty border of constant size for all the components
					 */ 
						

					
					
													
					
					containerPanel.add(Box.createRigidArea(new Dimension(0, 15)));
					
					/**
					 * adding of the components for the container
					 */ 
		
			containerPanel.add(Box.createRigidArea(new Dimension(0, 20)));
			
			
			containerPanel.add(labelNom);
			/**
			 *  making a space between  the label and the text field
			 *  
			 */ 
			
			/**
			 * display all the components: the text field, the buttons...
			 */
			
			
			containerPanel.add(Box.createRigidArea(new Dimension(0, 10)));
			containerPanel.add(textFieldNom);
			containerPanel.add(labelPrenom);
			containerPanel.add(Box.createRigidArea(new Dimension(0, 10)));
			containerPanel.add(textFieldPrenom);
			containerPanel.add(Box.createRigidArea(new Dimension(0, 10)));
			containerPanel.add(labelQualification);
			containerPanel.add(Box.createRigidArea(new Dimension(0, 10)));
			containerPanel.add(textFieldQualification);
			containerPanel.add(Box.createRigidArea(new Dimension(0, 10)));
			containerPanel.add(labelIdentifiant);
			containerPanel.add(Box.createRigidArea(new Dimension(0, 10)));
			containerPanel.add(textFieldIdentifiant);
			containerPanel.add(Box.createRigidArea(new Dimension(0, 10)));
			
			containerPanel.add(Box.createRigidArea(new Dimension(0, 10)));
			
			containerPanel.add(boutonQuitter);
			containerPanel.add(Box.createRigidArea(new Dimension(0, 10)));
			
			
			containerPanel.add(Box.createRigidArea(new Dimension(0, 10)));
			
		
	
			containerPanel.add(Box.createRigidArea(new Dimension(0, 10)));
			containerPanel.add(Box.createRigidArea(new Dimension(0, 10)));
			containerPanel.add(boutonSupprimer);
			containerPanel.add(Box.createRigidArea(new Dimension(0, 10)));
			containerPanel.add(boutonQuitter);
			containerPanel.add(Box.createRigidArea(new Dimension(0, 10)));
			
			
			/**
			 * adding of the actions concerning the buttons 
			 */ 
			boutonSupprimer.addActionListener(this);
			boutonQuitter.addActionListener(this);
		

			
			
			/**
			 * it allows to quit the application when we close the frame
			 */ 
			this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

			this.setContentPane(containerPanel);

			/**
			 * allows to display the frame
			 */ 
			this.setVisible(true);
		}

		
		public void actionPerformed(ActionEvent ae) {
			
			/**
			 *return code of the EnseignantDAO class
			 */
			int retour;

		try { 
			if (ae.getSource() == boutonQuitter) {
				this.dispose();	
			}
			else if (ae.getSource() == boutonSupprimer) {
				
				/**
				 * we create the message object
				 */ 
				Enseignant a = new Enseignant(
					this.textFieldNom.getText(),
					this.textFieldPrenom.getText(),
					this.textFieldQualification.getText(),
					this.textFieldIdentifiant.getText());
				
				
				/**
				 *we ask to the java class to send the teacher in the enseignant table 
				 */
				retour = monEnseignantSupprimerDAO.supprimer(a);
				
				/**
				 * displaying of the number of added lines in the BBD
				 */ 
				System.out.println("" + retour + " ligne supprimee ");
				if (retour == 1)
					JOptionPane.showMessageDialog(this, "enseignant  supprime !");
				else
					JOptionPane.showMessageDialog(this, "erreur suppression enseignant",
							"Erreur", JOptionPane.ERROR_MESSAGE);
				
			} /*else if (ae.getSource() == boutonAffichageTousLesEnseignants) {
				// on demande � la classe EnseignantDAO d'ajouter le message
				// dans la base de données
				List<Enseignant> liste = monEnseignantDAO.getListeEnseignant();
				// on efface l'ancien contenu de la zone de texte
				zoneTextListEnseignant.setText("");
				// on affiche dans la console du client les enseignants reçus
				for (Enseignant a : liste) {
					zoneTextListEnseignant.append(a.toString());
					zoneTextListEnseignant.append("\n");
					// Pour afficher dans la console :
					// System.out.println(a.toString());
				}*/
			
				
			} catch (Exception e) {
				JOptionPane.showMessageDialog(this,
						"Veuillez controler vos saisies", "Erreur",
						JOptionPane.ERROR_MESSAGE);
				System.err.println("Veuillez controler vos saisies");
			}
		}
		
	public static void main(String[] args) {
		new SupprimerEnseignant();
}

}



