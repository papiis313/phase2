import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Dimension;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;


import javax.swing.JTextField;
import javax.swing.JPanel;
import javax.swing.BoxLayout;
import javax.swing.Box;

/**
 *Authentification Class
* allows to insert the login and the password 
* 
*  
* 
*  
* @author TOURE_DIAGNE
* @version 1.3
* */


// la fenetre authentification permet � l'enseignant et � l'�tudiant de se connecter c'est � dire d'acc�der � leur espace scolaire






public class Authentification extends JFrame implements ActionListener {
	
	
	private static final long serialVersionUID = 1L;

	
	private JPanel containerPanel;

	/**
	 *text field for the login and the passeword
	 */ 
	private JTextField textFieldNom_utilisateur;
	private JTextField textFieldMot_de_pass;
	

	
/**
 * displaying the differents labels
 */ 

	
	private JLabel labelNom_utilisateur;
	private JLabel labelMot_de_pass;
	private JLabel labelMot_de_pass_oublie;
	

	
	
	/**
	 * buttons for the connexion and to quit the application
	 */ 
	
	private JButton boutonConnexion;
	
	private JButton boutonQuitter;
	

	
	
	
	
	

	public Authentification() {
		
		// on fixe le titre de la fen�tre
		this.setTitle("Authentification");
		// initialisation de la taille de la fen�tre
		this.setSize(300, 300);

		
		containerPanel = new JPanel();

	
		containerPanel.setLayout(new BoxLayout(containerPanel,
				BoxLayout.PAGE_AXIS));
	
		
		
		// choix de la couleur pour le conteneur
		
		containerPanel.setBackground(Color.white);
		// instantiation des composants graphiques
		
		
	
		textFieldNom_utilisateur = new JTextField();
		textFieldMot_de_pass = new JTextField();
		

		
		
		boutonConnexion = new JButton("Connexion");
		boutonQuitter=new JButton("Quitter");
		
		
		
		labelNom_utilisateur = new JLabel("Nom d'utilisateur");
		labelMot_de_pass = new JLabel("Mot de passe");
		labelMot_de_pass_oublie= new JLabel("mot de passe oublie?");
		
		
		// introduire une espace entre le label et le champ texte
		
		containerPanel.add(Box.createRigidArea(new Dimension(0, 5)));
				
	
		containerPanel.add(Box.createRigidArea(new Dimension(0, 15)));
		
		containerPanel.add(labelNom_utilisateur);
		containerPanel.add(Box.createRigidArea(new Dimension(0, 5)));
		containerPanel.add(textFieldNom_utilisateur);
		containerPanel.add(Box.createRigidArea(new Dimension(0, 10)));
		
		
		containerPanel.add(labelMot_de_pass);
		containerPanel.add(Box.createRigidArea(new Dimension(0, 10)));
		containerPanel.add(textFieldMot_de_pass);
		containerPanel.add(Box.createRigidArea(new Dimension(0, 10)));
		containerPanel.add(Box.createRigidArea(new Dimension(0, 10)));
		
		containerPanel.add(labelMot_de_pass_oublie);
		containerPanel.add(Box.createRigidArea(new Dimension(0, 5)));
		

		
		
	
		// ajouter une bordure vide de taille constante autour de l'ensemble des
				// composants
				containerPanel.setBorder(BorderFactory
						.createEmptyBorder(10, 10, 10, 10));

				// ajout des �couteurs sur les boutons pour g�rer les �v�nements
				//boutonOkProduit.addActionListener(this);

		
		// ajout des composants sur le container
		// introduire une espace constant entre le label et le champ texte
		containerPanel.add(Box.createRigidArea(new Dimension(0, 5)));
		
		// introduire une espace constant entre le champ texte et le composant
		// suivant
		
		containerPanel.add(Box.createRigidArea(new Dimension(0, 10)));
		containerPanel.add(boutonConnexion);
		containerPanel.add(Box.createRigidArea(new Dimension(0, 20)));
		containerPanel.add(boutonQuitter);
		
		

		// ajouter une bordure vide de taille constante autour de l'ensemble des
		// composants
		containerPanel.setBorder(BorderFactory
				.createEmptyBorder(10, 10, 10, 10));
		// ajout des �couteurs sur les boutons pour g�rer les �v�nements
		boutonConnexion.addActionListener(this);
		
		boutonQuitter.addActionListener(this);
		
		
		// permet de quitter l'application si on ferme la fen�tre
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		this.setContentPane(containerPanel);

		// affichage de la fen�tre
		this.setVisible(true);
	}

	/**
	 * G�re les actions r�alis�es sur les boutons
	 *
	 */
	public void actionPerformed(ActionEvent ae) {
		//int retour;
		// code de retour de la classe OperateurDAO

		try {
			if (ae.getSource() == boutonConnexion) {
				this.dispose();
				new Fonctionnalites();
				
				
				
			} 
			else if(ae.getSource() ==boutonQuitter){
				this.dispose();
			}
		} catch (Exception e) {
			JOptionPane.showMessageDialog(this,
					"Veuillez contr�ler vos saisies", "Erreur",
					JOptionPane.ERROR_MESSAGE);
			System.err.println("Veuillez contr�ler vos saisies");
		}

	}

	public static void main(String[] args) {
		new Authentification();
	}

}


