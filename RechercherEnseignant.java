
	import java.awt.Color;
	import java.awt.event.ActionEvent;
	import java.awt.event.ActionListener;
	import java.awt.Dimension;

	import javax.swing.BorderFactory;
	import javax.swing.JButton;
	import javax.swing.JFrame;
	import javax.swing.JLabel;
	import javax.swing.JOptionPane;
	import javax.swing.JScrollPane;
	import javax.swing.JTextArea;
	import javax.swing.JTextField;
	import javax.swing.JPanel;
	import javax.swing.BoxLayout;
	import javax.swing.Box;
	import java.util.List;



	/**
	 * Classe EnseignantFenetre
	 * D�finit et ouvre une fenetre qui :
	 * 
	 *    - Permet l'insertion d'un nouvel enseignant dans la table enseignant via
	 * la saisie des valeurs du nom, prenom, la qualification et Id
	 *    - Permet l'affichage de tous les enseignants dans la console
	 * @author hawa Toure
	 * @version 1.3
	 * */


	public class RechercherEnseignant extends JFrame implements ActionListener {
		/**
		 * numero de version pour classe serialisable Permet d'eviter le warning
		 * "The serializable class EnseignantFenetre does not declare a static final serialVersionUID field of type long"
		 */
		private static final long serialVersionUID = 1L;

		/**
		 * conteneur : il accueille les differents composants graphiques de
		 * EnseignantFenetre
		 */
		private JPanel containerPanel;

		
		private JTextField textFieldId;


		
		private JLabel labelId;
		

		
	/**
		 * bouton d'ajout de l'enseignant
		 */
		private JButton boutonQuitter;
		private JButton boutonRechercher;
		private JButton boutonAffichageTousLesEnseignants;
		

		
		JTextArea zoneTextRechercheEnseignant;

		/**
		 * Zone de d�filement pour la zone de texte
		 */
		JScrollPane zoneDefilement;

		/**
		 * instance de OperateurDAO permettant les associatons� la base de données
		 */
		private EnseignantDAO monEnseignantDAO;

		/**
		 * Constructeur D�finit la fen�tre et ses composants - affiche la fen�tre
		 */
		public RechercherEnseignant() {
			// on instancie la classe EnseignantDAO
			this.monEnseignantDAO = new EnseignantDAO();

			// on fixe le titre de la fen�tre
			this.setTitle("Menu de recherche d'enseignant");
			// initialisation de la taille de la fenêtre
			this.setSize(400, 400);

			// crtion du conteneur
			containerPanel = new JPanel();

			// choix du Layout pour ce conteneur
			// il permet de g�rer la position des �l�ments
			// il autorisera un retaillage de la fen�tre en conservant la
			// pr�sentation
			// BoxLayout permet par exemple de positionner les élements sur une
			// colonne ( PAGE_AXIS )
			containerPanel.setLayout(new BoxLayout(containerPanel,
					BoxLayout.PAGE_AXIS));
			// choix de la couleur pour le conteneur
			containerPanel.setBackground(Color.pink
					);

			new JTextField();
			textFieldId = new JTextField();
			
			
			
			
			
			boutonQuitter= new JButton("Quitter");
			boutonRechercher = new JButton("Rechercher");
			
			
			boutonAffichageTousLesEnseignants = new JButton(
					"Afficher tous les enseignants");
			
			
			
			

			labelId=new JLabel("Id:");
			
			
			

			zoneTextRechercheEnseignant = new JTextArea(10, 20);
			zoneDefilement = new JScrollPane(zoneTextRechercheEnseignant);
			zoneTextRechercheEnseignant.setEditable(false);

			
			
			// introduire une espace constant entre le label et le champ texte
					containerPanel.add(Box.createRigidArea(new Dimension(0, 5)));
					
			
			// ajouter une bordure vide de taille constante autour de l'ensemble des
					// composants
					containerPanel.setBorder(BorderFactory
							.createEmptyBorder(10, 10, 10, 10));

					
					
					containerPanel.setBackground(Color.white);
													
					
					containerPanel.add(Box.createRigidArea(new Dimension(0, 15)));
					
					// ajout des composants sur le container
			
			// introduire une espace constant entre le champ texte et le composant
			// suivant
		
			containerPanel.add(Box.createRigidArea(new Dimension(0, 10)));
			containerPanel.add(labelId);
			containerPanel.add(Box.createRigidArea(new Dimension(0, 5)));
			containerPanel.add(textFieldId);
			containerPanel.add(Box.createRigidArea(new Dimension(0, 10)));
			containerPanel.add(Box.createRigidArea(new Dimension(0, 10)));
			containerPanel.add(boutonRechercher);
		
			containerPanel.add(Box.createRigidArea(new Dimension(0, 30)));
			
			containerPanel.add(boutonQuitter);
			containerPanel.add(Box.createRigidArea(new Dimension(0, 5)));
			containerPanel.add(zoneDefilement);
			// ajouter une bordure vide de taille constante autour de l'ensemble des
			// composants
			containerPanel.setBorder(BorderFactory
					.createEmptyBorder(10, 10, 10, 10));
			// ajout des écouteurs sur les boutons pour g�rer les �v�nements
			boutonRechercher.addActionListener(this);
			boutonAffichageTousLesEnseignants.addActionListener(this);

			
			
			// permet de quitter l'application si on ferme la fen�tre
			this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

			this.setContentPane(containerPanel);

			// affichage de la fen�tre
			this.setVisible(true);
		}

		/**
		 * G�rer les actions r�alisables sur les boutons
		 *
		 */
		public void actionPerformed(ActionEvent ae) {
			int retour=0; // code de retour de la classe EnseignantDAO

		try {
				if (ae.getSource() == boutonRechercher) {
					
					String a = this.textFieldId.getText();
					Enseignant enseignant = monEnseignantDAO.rechercher(a);
					zoneTextRechercheEnseignant.setText("");
					zoneTextRechercheEnseignant.append(enseignant.toString());
					System.out.println(""+ retour+"ligne trouv�");
		
				// on cr�e l'objet message
		
					
					//this.textFieldId.getText();
					// on demande � la classe de communication d'envoyer l'enseignant
					// dans la table enseignant
						// retour = monEnseignantDAO.rechercher(this.textFieldId.getText());
					// affichage du nombre de lignes ajout�es
					// dans la bdd pour v�rification
					//System.out.println("" + retour + " ligne trouv�");
					//if (retour == 1)
					//JOptionPane.showMessageDialog(this, "enseignant  trouv�e !");
						//else
					//JOptionPane.showMessageDialog(this, "erreur ajout enseignant",
								//		"Erreur", JOptionPane.ERROR_MESSAGE);
				 
				 /* else if (ae.getSource() == boutonAffichageTousLesEnseignants) {
					// on demande � la classe EnseignantDAO d'ajouter le message
					// dans la base de données
					List<Enseignant> liste = monEnseignantDAO.getListeEnseignant();
					// on efface l'ancien contenu de la zone de texte
					zoneTextListEnseignant.setText("");
					// on affiche dans la console du client les enseignants reçus
					for (Enseignant a : liste) {
						zoneTextListEnseignant.append(a.toString());
						zoneTextListEnseignant.append("\n");
						// Pour afficher dans la console :
						// System.out.println(a.toString());
					}*/
				}
			} catch (Exception e) {
				JOptionPane.showMessageDialog(this,
						"Veuillez controler vos saisies", "Erreur",
						JOptionPane.ERROR_MESSAGE);
				System.err.println("Veuillez controler vos saisies");
			}

		}
	public static void main(String[] args) {
		new RechercherEnseignant();
	}

	}









